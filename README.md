BCP
===

BCP is an implementation of a branch-and-cut-and-price model of the multi-agent path finding problem. This source code repository has moved to <https://github.com/ed-lam/bcp-mapf>.

Authors
-------

BCP is invented by Edward Lam with assistance from Pierre Le Bodic, Daniel Harabor and Peter J. Stuckey. Edward can be reached at [ed-lam.com](https://ed-lam.com).